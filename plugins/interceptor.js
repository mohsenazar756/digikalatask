export default function ({ $axios, redirect }) {
  $axios.interceptors.request.use(
    function (config) {
      return config;
    },
    function (error) {
      console.log(error);
      return Promise.reject(error);
    }
  );

  $axios.interceptors.response.use(
    function (response) {
      console.log(response);
      return response;
    },
    function (error) {
      console.log(error);
      return Promise.reject(error);
    }
  );
}
