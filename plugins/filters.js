import Vue from 'vue';

Vue.filter('currency', function (value) {
  if (value) {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
  return value;
});
