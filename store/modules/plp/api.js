
export const getPlpData = async (parent, data) => {
  const _this = parent;
  const res = await _this.$axios.$get('/api/search/', {
    params: data
  })
  return res;
};