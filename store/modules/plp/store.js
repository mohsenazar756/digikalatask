import {
  getPlpData
} from './api';

const state = {
  plpStatus: '',
  getPlp: ''
};

const getters = {};

const mutations = {
  PLP_STATUS(state, plpStatus) {
    state.plpStatus = plpStatus
  },
  GET_PLP(state, getPlp) {
    state.getPlp = getPlp
  }
};

const actions = {
  getPlp(context, payload) {
    context.commit('PLP_STATUS', 'pending')
    getPlpData(this, payload).then((result) => {
      if (result.status === 200) {
        context.commit('PLP_STATUS', 'success');
        context.commit('GET_PLP', result.data);
      }
    })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
