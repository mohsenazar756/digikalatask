
const state = {
  myCart: []
};

const getters = {};

const mutations = {
  ADD_TO_CART(state, myCart) {
    if(state.myCart.length) {
      let duplicate = state.myCart.filter(i => i.id === myCart.id)
      !duplicate.length ? state.myCart.push(myCart) : ''
    } else state.myCart.push(myCart)
    $cookies.set('cart', JSON.stringify(state.myCart), '2d')
  },
  DELETE_CART_ITEM(state, id) {
    state.myCart = state.myCart.filter(i => i.id !== id)
    $cookies.set('cart', JSON.stringify(state.myCart), '2d')
  },
  SET_CART_FROM_COOKIE(state, myCart) {
    state.myCart = myCart
  }
};

const actions = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
