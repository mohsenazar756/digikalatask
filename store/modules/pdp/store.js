import {
  getPdpData,
} from './api';

const state = {
  getPdp: ''
};

const getters = {};

const mutations = {
  GET_PDP(state, getPdp) {
    state.getPdp = getPdp
  }
};

const actions = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
