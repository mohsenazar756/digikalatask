import Vue from 'vue';
import Vuex from 'vuex';

import plp from './modules/plp/store';
import pdp from './modules/pdp/store';
import cart from './modules/cart/store';
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.use(Vuex);
const createStore = () => {
  return new Vuex.Store({
    modules: {
      plp,
      pdp,
      cart
    }
  });
};
export default createStore;
