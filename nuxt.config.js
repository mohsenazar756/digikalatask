export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'digitask',
    htmlAttrs: {
      dir: 'rtl',
      lang: 'fa'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loadingIndicator: {
    name: 'rectangle-bounce',
    color: '#fb3449',
    background: 'white'
  },

  loading: { color: '#d900ba', throttle: 200, height: '2px', css: true },

  axios: {
    proxy: true,
    headers: {
      'token': 'mpfKW9ghVTCSuBZ7qTkSmEyvL38ShZxv',
    }
  },

  proxy: {
    '/api': {
      target: 'https://www.digikala.com/front-end',
      pathRewrite: {
        '^/api': ''
      },
      changeOrigin: true
    }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios.js',
    { src: '~/plugins/filters.js', ssr: true },
    { src: '~/plugins/interceptor.js', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    [
      'nuxt-fontawesome', {
        imports: [
         {
           set: '@fortawesome/free-solid-svg-icons',
           icons: ['fas']
         },
         {
           set:'@fortawesome/free-brands-svg-icons',
           icons: ['fab']
         }
       ]
      }
    ]
  ],

  i18n: {
    locales: ['fa'],
    defaultLocale: 'fa',
    vueI18n: {
      fallbackLocale: 'fa',
      messages: {
        fa: require('./static/i18n/fa.json')
      }
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'nuxt-i18n'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
